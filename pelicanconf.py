#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals



AUTHOR = 'Jérémy Bois'
SITENAME = 'AnotherJourney'
SITEURL = ''
PATH = 'content'

# Make sure modifications are visible
LOAD_CONTENT_CACHE = False

STATIC_PATHS = ['images', 'pdfs']
DIRECT_TEMPLATES = ['keywords', 'index', 'categories', 'tags', 'search']
RELATIVE_URLS = False

TEMPLATE_PAGES = {'templates/wiki.html': 'wiki/index.html'}

MENUTARGETS = (
    ('Home', '/'),
    ('About', '/about/'),
    ('Wiki', '/wiki/'),
    ('Mots-clés', '/keywords/'),
)


# Articles organization
ARTICLE_ORDER_BY = 'reversed-modified'
DEFAULT_ORPHANS = 2
DEFAULT_PAGINATION = 5


# Do not publish articles set in the future
WITH_FUTURE_DATES = False

# Post summary size
SUMMARY_MAX_LENGTH = 50

# Other
DEFAULT_METADATA = {
    'status': 'draft',
}


#
# OUTPUT FOLDER ORGANIZATION
#
SLUGIFY_SOURCE = 'basename'

# How to save articles
ARTICLE_URL = 'posts/{date:%Y}/{date:%b}/{slug}/'
ARTICLE_SAVE_AS = ARTICLE_URL + 'index.html'
ARTICLE_PATHS = ['posts']

# Static pages
PAGE_URL = '{slug}/'
PAGE_SAVE_AS = PAGE_URL + 'index.html'
PAGE_PATHS = ['pages']

# Category
CATEGORY_URL = 'category/{slug}/'
CATEGORY_SAVE_AS = CATEGORY_URL + 'index.html'

# Tags
TAG_URL = 'tag/{slug}/'
TAG_SAVE_AS = TAG_URL + 'index.html'

# # Tags, categories and archives are Direct Templates, so they don't have a
# # <NAME>_URL option used directly by Pelican but one can still be defined and used ...
TAGS_URL = '/tags/'
TAGS_SAVE_AS = 'tags/index.html'
CATEGORIES_URL = '/categories/'
CATEGORIES_SAVE_AS = 'categories/index.html'
KEYWORDS_SAVE_AS = 'keywords/index.html'

# Archives
YEAR_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/{date:%b}/index.html'
# Not working
# ARCHIVES_SAVE_AS = 'posts/index.html'

# Where to find the theme
THEME = "theme"
# No image for posts
POST_IMAGE = ""
PROFILE_IMAGE = "base/logoCrow.png"


# Localization
TIMEZONE = 'Europe/Paris'
DEFAULT_LANG = 'fr'
ARTICLE_LANG_SAVE_AS = None
PAGE_LANG_SAVE_AS = None
DEFAULT_DATE_FORMAT = '%a %d %B %Y'
LOCALE = ('fr_FR.utf8', 'fr_FR.UTF-8', 'fr_FR', 'fr')

# Feeds
FEED_DOMAIN = SITEURL
FEED_ALL_ATOM = None
FEED_ALL_RSS = None
CATEGORY_FEED_ATOM = None
CATEGORY_FEED_RSS = None
TRANSLATION_FEED_ATOM = None
TRANSLATION_FEED_RSS = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Social
SOCIAL = [("Github", "https://github.com/JeremyBois/"),
          ("Gitlab", "https://gitlab.com/JeremyBois/"),
          ("Bitbucket", "https://bitbucket.org/JeremyBois/")]

# PLUGINS
PLUGIN_PATHS = ['plugins']
PLUGINS = ['tipue_search', 'share_post']

TIPUE_SEARCH = True
TYPOGRIFY = True


# Jupyter files (require nbconvert < 6.0)
from pelican_jupyter import liquid as nb_liquid
PLUGINS.append(nb_liquid)
# Options
MARKUP = ('md', )
IGNORE_FILES = [".ipynb_checkpoints"]
IPYNB_SKIP_CSS = True
LIQUID_CONFIGS = (
    ("IPYNB_SKIP_CSS", "True", ""),
)
