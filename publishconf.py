#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)

from pelicanconf import *

# Config
RELATIVE_URLS = False
DELETE_OUTPUT_DIRECTORY = True

# In / Out
SITEURL = 'https://jeremybois.gitlab.io/{0}'.format(SITENAME)
PATH = 'content'
OUTPUT_PATH = 'public'

# Feed
FEED_DOMAIN = SITEURL
FEED_RSS = 'feed/index.html'
FEED_ATOM = 'feed/atom/index.html'
FEED_ALL_ATOM = 'feeds/all.atom.xml'
FEED_ALL_RSS = 'feeds/all.rss.xml'

# Tag (prefixed with SITENAME needed when host in gitlab)
# Needed because tags are rendered using macro
# Don't no why this is needed ...
TAG_URL = SITENAME + '/tag/{slug}/'
TAG_SAVE_AS = 'tag/{slug}/' + 'index.html'

CATEGORY_URL = SITENAME + '/category/{slug}/'
CATEGORY_SAVE_AS = 'category/{slug}/' + 'index.html'
