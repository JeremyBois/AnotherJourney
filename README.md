---
title: README
---

[![pipeline status](https://gitlab.com/JeremyBois/AnotherJourney/badges/master/pipeline.svg)](https://gitlab.com/JeremyBois/AnotherJourney/commits/master)


# Hosting

Currently host as a gitlab static page without custom domain.
[Go to site](https://jeremybois.gitlab.io/AnotherJourney)


# Infos
## Theme

Based on [Hyde](https://github.com/jvanz/pelican-hyde)
with a lot of modifications.


## Plugins

  - Improve typography:  `Typogrify`
  - Search the website: `tipue_search`
  - Share post to social website without tracking: `share_post`




## Dev Build

Site can be found at **http://localhost:8000**.

`pelican --listen` or `pelican -l`

or during development (content reloaded when source change)

`pelican -lr`


## Prod Build

Site can be found at **https://jeremybois.gitlab.io/AnotherJourney**.

```pelican -s publishconf.py```


## Design

  - [Pelican](http://docs.getpelican.com/en/stable/index.html)
  - [Colors picker](http://paletton.com/)
  - [Pattern](https://www.toptal.com/designers/subtlepatterns)
  - [Support des navigateurs](https://caniuse.com/)
  - [Icons](https://forkawesome.github.io/Fork-Awesome/)
