title: XFCE4 comme environnement de bureau !
slug: Xfce4
category: GNU-Linux
tags: arch, xfce, gnu-linux
date: 2020-10-09
modified: 2020-10-09
image: posts/xfce4.png
status: published


**XFCE4** est simple et consomme très peu de ressources. Il a aussi l'avantage de conserver la session
courante permettant de rapidement avoir les applications prêtes dès le démarrage de l'ordinateur.
Il est cependant nécessaire de faire quelques modifications pour avoir un environnement qui me convient.


### Installation
```bash
    sudo pacman -Syu xfce4 xfce-notifyd xfce-pulseaudio-plugin xfce-power-manager gnome-terminal
    # Remove default xfce terminal
    sudo pacman -Rs xfce-terminal
```

### Thèmes
J'utilise la configuration suivante:
  - Icônes: [Milix](https://www.cinnamon-look.org/p/1259356/)
  - Curseurs: [Tux cursors](https://www.cinnamon-look.org/p/999945/)
  - Fenêtres: [Feren](https://github.com/feren-OS/feren-theme/tree/master/feren)
  - Apparences: Adwaita (disponible par défaut)

Pour les polices j'utilise [**Nunito Sans Regular**](https://fonts.google.com/specimen/Nunito+Sans) par défaut
et [**mononoki-Regular Nerd Font Complete Mono**](https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/Mononoki/Regular/complete/mononoki-Regular%20Nerd%20Font%20Complete%20Mono.ttf)
en police monospace. (pour savoir pourquoi c'est par [ici]({filename}/posts/OhMyGit.md))


### Gestionnaire de dossiers:
Pour la navigation dans les dossier j'utilise *thunar-split*(AUR) qui ajoute la possibilité de diviser en deux
le panneau avec `F3`. Comme il remplace le gestionnaire de base il reste compatible avec les extensions du dépôt officiel.
Pour pouvoir gérer les archives il est nécessaire d'ajouter un plugin et un gestionnaire d'archives:

```bash
    pakku -S thunar-split
    sudo pacman -Syu file-roller thunar-archive-plugin
```


### Super comme modificateur
Xfce ne permet pas d'utiliser par défaut une touche autre que **alt** comme un modificateur.
Comme j'aime bien avoir accès au menu en utilisant la touche **Super-L** et modifier l'ancrage des
fenêtres avec **Super-L + left/right/up/down** j'ai installé le logiciel suivant [superkey](https://github.com/JixunMoe/xfce-superkey)
qui est disponible directement depuis le dépôt utilisateur: `pakku -S xfce-superkey-git`


### Contrôler l'éclairage de l'écran
Pour contrôler la luminosité de l'écran j'utilise des raccourcis personnalisés car les touches **Fn+F3** et **Fn+F4** ne sont pas
reconnues:
  - `xbacklight -dec 10 -steps 5` **Ctrl + F3**
  - `xbacklight -inc 10 -steps 5` **Ctrl + F4**

Pour savoir comment faire pour faire fonctionner *xbacklight* si on utilise le driver *modesetting* c'est par [ici](({filename}/posts/acpilight.md)).


### Autres
Pour bloquer la pavé tactile j'utilise le script d'un [pour les détail c'est par ici...]({filename}/posts/ToggleTouchpad.md)
et la touche **Pause**.

Pour lancer *rofi*, un lanceur d'application et bien plus ([plus d'infos par ici]({filename}/posts/ToggleTouchpad.md),
j'utilise **Alt + F2**



***
Et voilà !
