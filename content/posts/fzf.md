title: Rechercher efficacement avec fzf
slug: fzfFuzzySearch
category: Tool
tags: astuce, gnu-linux, terminal
date: 2020-10-09
modified: 2020-10-09
image: posts/fzf.jpg
image_title: Background vector created by vectorgraphit - www.freepik.com
status: published


Je passe pas mal de temps dans le terminal et ce petit outil est formidable.
La configuration de base permet déjà pas mal de choses mais l'outil libère son potentiel une fois
que l'on met les mains dans le cambouis ...


Pour l'installer, rien de plus simple: `sudo pacman -Rs noto-fonts-emoji fzf`. La police ajoutée n'est pas
strictement nécessaire mais elle permet d'ajouter des émoticons à la configuration (🌵 🦉).

L'ensemble de la configuration doit être ajouté dans le `~/.bashrc` (voir ci-dessous):

  - `1)`: Définition de la mise en forme des résultats de recherche ainsi que la manière dont est faite la recherche
  - `2)`: Gestion de l'autocomplétion et de la mise en forme en fonction de l'application
    - Il faut presser ` **<Tab>` après la commande (*subl3*, *less*, ...) pour activer la recherche floue
    - La manière de rechercher et la mise en forme sont propres à chaque commande
  - `3)`: Activer et spécialisé les raccourcis pour:
    - **Ctrl+T** Rechercher depuis la racine des fichiers ou des dossiers
    - **Ctrl+R** Rechercher dans l'historique des commandes rapidement
    - **ALt+C** Changer rapidement de dossier source


```bash
    # 1) SETUP DEFAULT PARAMETERS
    export FZF_DEFAULT_COMMAND='fd --hidden --exclude ".git" --exclude ".svn" .'
    export FZF_DEFAULT_OPTS="--height 80% --no-reverse --tiebreak=length,end,index --header=\"🌵   🦉   🌵\""


    # 2) ADD AUTO-COMPLETION
    source /usr/share/fzf/completion.bash
    # Specific control on how results are shown
    # Add support for sublime text, evince, nvim (based on current path)
    _fzf_setup_completion path subl3 evince nvim less
    _fzf_comprun() {
      local command=$1
      shift
      case "$command" in
        cd)           fzf "$@" --preview-window right:50%:border --preview 'tree -C {} | head -200' ;;
        subl3)        fzf "$@" --preview-window right:50%:border --preview '(cat {} || tree -C {}) 2> /dev/null | head -200' ;;
        nvim)         fzf "$@" --preview-window right:50%:border --preview '(cat {} || tree -C {}) 2> /dev/null | head -200' ;;
        less)         fzf "$@" --preview-window right:50%:border --preview '(cat {} || tree -C {}) 2> /dev/null | head -200' ;;
        export|unset) fzf "$@" --preview-window right:50%:border --preview "eval 'echo \$'{}" ;;
        ssh)          fzf "$@" --preview-window right:50%:border --preview 'dig {}' ;;
        *)            fzf "$@" ;;
      esac
    }
    # Setup how folders are found
    _fzf_compgen_dir() {
      fd --type d --hidden --exclude ".git" --exclude ".svn" . "$1"
    }
    # Setup how files are found
    _fzf_compgen_path() {
      fd --hidden --exclude ".git" --exclude ".svn" . "$1"
    }


    # 3) ADD KEYBINDINGS SUPPORT
    source /usr/share/fzf/key-bindings.bash # arguments for fzf
    # CTRL-R --> History search using fuzzy search
    export FZF_CTRL_R_OPTS="--preview 'echo {}' --preview-window down:3:hidden:wrap --bind '?:toggle-preview'"
    # CTRL-T --> Search file and folder inside root from any location
    export FZF_CTRL_T_COMMAND="fd --hidden --exclude '.git' --exclude '.svn' . /"
    export FZF_CTRL_T_OPTS="--preview-window right:50%:border --preview '(cat {} || tree -C {}) 2> /dev/null | head -200'"
    # ALT-C  --> CD in any folder inside root from any location
    export FZF_ALT_C_COMMAND="fd --type d --hidden --exclude '.git' --exclude '.svn' . /"
    export FZF_ALT_C_OPTS="--preview-window right:50%:border --preview '(tree -C {}) 2> /dev/null | head -200'"
```

Avec ce petit outil, la manipulation des dossiers et des fichiers dans le terminal devient très rapide et efficace.


***
Et voilà !
