title: A better xbacklight !
slug: Acpilight
category: Tool
tags: astuce, gnu-linux
date: 2020-10-08
modified: 2020-10-08
image: posts/acpilight.jpg
image_title: http://www.jamiesale-cartoonist.com/free-cartoon-octopus-clip-art-vector/
status: draft



J'utilisais xbackight pour gérer la luminosité de mon écran. Cependant celui-ci ne fonctionne que
si le driver *intel* est utilisé alors que j'utilise deux configurations distinctes:
  - *nvidia + modesetting* pour *cuda* et faire tourner les jeux gourmands
  - *intel* pour maximiser l'autonomie de mon portable

Il existe cependant une alternative ([acpilight](https://gitlab.com/wavexx/acpilight)) en attendant que la correction soit faite directement dans le logiciel.
`sudo pacman -Syu acpilight`

Une fois installé, il remplace *xbacklight* de manière transparente. Pour pouvoir l'utiliser sans être root, la manière
la plus simple et celle explicité dans le Readme du projet.

Les étapes sont les suivantes:

  - Ajouter l'utilisateur au groupe *video*: `sudo gpasswd -a <User> video`
  - Modifier les permissions avec le contenu ci-dessous: `sudo subl3 /etc/udev/rules.d/90-backlight.rules`

```
    SUBSYSTEM=="backlight", ACTION=="add", \
      RUN+="/bin/chgrp video /sys/class/backlight/%k/brightness", \
      RUN+="/bin/chmod g+w /sys/class/backlight/%k/brightness"

```

Il suffit maintenant de redémarrer ou de forcer la mise à jour des règles avec `sudo udevadm control --reload`.


***
Et voilà !
