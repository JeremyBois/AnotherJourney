title: Intégrer Git au terminal
slug: ohMyGit
category: Tool
tags: astuce, tool, terminal, gnu-linux
date: 2020-10-08
modified: 2020-10-08
image: posts/oh-my-git.png
image_title: http://www.jamiesale-cartoonist.com/free-cartoon-octopus-clip-art-vector/
status: published




Je gère toujours mes dépôt git depuis le terminal. J'ai jamais été fan des GUI et j'aime bien
savoir quelle commande a été lancé, les automatiser, ...
Ce que j'aime bien aussi c'est avoir un maximum d'informations d'un simple regard et
[*oh-my-git*](https://github.com/arialdomartini/oh-my-git) est parfait pour ça !


Dans un premier temps il est nécessaire d'installer une police qui est patchée pour supporter
les charactères exotiques comme le propose [Nerd font](https://www.nerdfonts.com/).
Utilisant *mononoki* dans mon terminal j'ai téléchargé la version patchée correspondante (voir code ci-dessous).
Ensuite on récupère les sources de *oh-my-git*.


```bash
    # Installation de la police
    curl -o ~/.fonts/mononoki-Regular\ Nerd\ Font\ Complete\ Mono.ttf https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/Mononoki/Regular/complete/mononoki-Regular%20Nerd%20Font%20Complete%20Mono.tt
    # On récupère les sources de oh-my-git
    cd && git clone https://github.com/arialdomartini/oh-my-git.git .oh-my-git
```

Il reste à modifier son `~/.bashrc` afin de l'activer.

```bash
    # Better handling of python environment prompt
    VIRTUAL_ENV_DISABLE_PROMPT=true
    function omg_prompt_callback() {
    if [ -n "${VIRTUAL_ENV}" ]; then
        echo "\e[0;31m(`basename ${VIRTUAL_ENV}`)\e[0m "
    fi
    }
    # Activate oh-my-git
    source /home/pampi/.oh-my-git/prompt.sh
```

Maintenant à chaque fois que l'on est dans un dépôt git, de nouvelles informations seront disponibles !

Il est aussi possible de choisir ces propres symboles pour les différentes informations en modifiant le fichier `~/.oh-my-git/prompt.sh`.
J'utilise les symboles suivants:

```bash
    : ${omg_is_a_git_repo_symbol:=''}
    : ${omg_has_untracked_files_symbol:='懲'}        #                ?    
    : ${omg_has_adds_symbol:=''}
    : ${omg_has_deletions_symbol:=''}
    : ${omg_has_cached_deletions_symbol:=''}
    : ${omg_has_modifications_symbol:=''}
    : ${omg_has_cached_modifications_symbol:=''}
    : ${omg_ready_to_commit_symbol:=''}            #   →
    : ${omg_is_on_a_tag_symbol:=''}                #   
    : ${omg_needs_to_merge_symbol:='ᄉ'}
    : ${omg_detached_symbol:=''}
    : ${omg_can_fast_forward_symbol:=''}
    : ${omg_has_diverged_symbol:=''}               #   
    : ${omg_not_tracked_branch_symbol:=''}
    : ${omg_rebase_tracking_branch_symbol:=''}     #   
    : ${omg_merge_tracking_branch_symbol:=''}      #  
    : ${omg_should_push_symbol:=''}                #    
    : ${omg_has_stashes_symbol:=''}

```


***
Et voilà !
