title: Arch Linux sur l'Aero-15X
slug: Arch with Aero15X
category: GNU-Linux
tags: arch, config, aero15x, gnu-linux
date: 2019-08-25
modified: 2020-05-09
image: posts/ArchAero.png
status: published


### Pré-requis
#### Support 32bits
Il sera nécessaire d'avoir le support pour les bibliothèques 32 bits pour steam, wine, ...
Il faut donc ajouter le dépôt pour pacman: `sudo nano /etc/pacman.conf` et décommenter pour avoir
```
    [multilib]
    Include = /etc/pacman.d/mirrorlist
```

#### Logs
```bash
    sudo pacman -Syu syslog-ng
```

Pour pouvoir déboguer un problème il est plus simple d'avoir les logs en clair.

  - `sudo nano /etc/systemd/journald.conf`
  - Remplacer `#ForwardToSyslog=yes` par `ForwardToSyslog=yes`
  - Activer le service `sudo systemctl enable syslog-ng@default`



***
### Environnement graphique
#### Xorg
([Doc](https://wiki.archlinux.org/index.php/Xorg))

C'est le minimum requis pour avoir une session graphique: `sudo pacman -Syu xorg`.


#### Cinnamon
Ajout de l'environnement de bureau (**Cinnamon**) + gestionnaire de login (**lightdm**):
```bash
    sudo pacman -Syu cinnamon lightdm lightdm-gtk-greeter
    sudo pacman -Syu gnome-terminal tilix nemo-fileroller xdg-user-dirs gtk-engine-murrine
    # Mets en place les dossiers utilisateurs et les liens symboliques pour les applications
    xdg-user-dirs-update
```



***
### AUR Helper
See [AUR helper](https://wiki.archlinux.org/index.php/AUR_helpers)

```bash
    cd ~/Downloads/
    git clone https://aur.archlinux.org/pakku.git pakku && cd pakku
    makepkg -si
    cd ../ && rm -R pakku

```



***
### Fonts:
`sudo pacman -Syu ttf-{bitstream-vera,liberation,dejavu,droid,anonymous-pro,inconsolata} freetype2 libotf gnome-font-viewer`

Pour mes petits yeux il faut aussi la police `Mononoki` téléchargeable à ce [lien](https://madmalik.github.io/mononoki/).
Ensuite il faut ajouter les polices du dossier fournit `Fonts` dans `~/.fonts`.



***
### Cartes graphiques
#### Nvidia / Intel
Voir [Doc Nvidia](https://wiki.archlinux.org/index.php/NVIDIA), [Doc Intel](https://wiki.archlinux.org/index.php/Intel),
[Doc drivers](https://wiki.archlinux.org/index.php/Xorg#Driver_installation).

`sudo pacman -Syu mesa vulkan-intel xf86-input-libinput nvidia nvidia-utils nvidia-settings lib32-nvidia-utils`

Pas besoin de fichiers de configuration, on va les construire en fonction de nos préférences !


#### Optimus
Voir [Doc Arch](https://wiki.archlinux.org/index.php/NVIDIA_Optimus#Using_optimus-manager).

Pour pouvoir switcher simplement entre la carte Nvidia et la carte Intel on installe *optimus-manager*: `pakku -S optimus-manager`
Il est possible de choisir entre trois modes: Intel, Nvidia, Hybrid: `sudo optimus-manager --switch <mode> --no-confirm`
(`--no-confirm` nécessaire pour éviter un message qui concerne que les cartes Nvidia de la génération suivante, les RTX).

Pour améliorer le temps de batterie on va faire plusieurs choses. D'abord modifier la configuration par
défaut en forçant la suppression de la carte Nvidia du bus PCI et autorisant la gestion de l'énergie en mode Intel.
Ensuite on va ajouter des options aux modules pour améliorer la gestion de l'énergie
([Doc Intel](https://wiki.archlinux.org/index.php/Intel_graphics#Enable_GuC_/_HuC_firmware_loading).


```bash
  cd Nvidia/Kernel-5.0
  sudo cp -R ./optimus-manager /etc/optimus-manager
  sudo cp /etc/optimus-manager/i915.conf /etc/modprobe.d/
```


Là ou c'est plus compliqué c'est pour la gestion de l'énergie de la carte Nvidia car le module *snd_hda_intel*
empêche de supprimer la carte du bus car il utilise la carte son de la carte graphique à partir du kernel 5.0 ...
L'astuce est donc d'empêcher le module de détecter la carte son lors de son initialisation.
Comme on veut améliorer la batterie lorsque uniquement la carte Intel est active et pouvoir avoir le son
par l'HDMI il faut que ce blocage soit dépendant du mode voulu.
Le script (`/etc/optimus-manager/nvidia_sound_card.sh`) doit être exécutable (`sudo chmod +x /etc/optimus-manager/nvidia_sound_card.sh`)
et deux alias ont été ajouté: `NvidiaProfile` et `IntelProfile` permettant respectivement de switcher vers le mode Nvidia avec le son
sur l'HDMI et vers le mode Intel sans le son vers l'HDMI mais la gestion de l'énergie sur le bus PCI de la carte graphique Nvidia améliorant
fortement la batterie (de 300-400%, oui oui d'autant).

À partir de *optimus-manager 1.3* la commande `--set-startup` est déprécié est c'est dans
le fichier de configuration que le mode au démarrage est défini. La solution ci-dessous
(`NvidiaProfile`) permet grâce à `--temp-config` de forcer uniquement pour le prochain
redémarrage de passer sur Nvidia et donc de simuler l'option `--set-startup`. Attention si
l'ordinateur est éteint normalement en étant sur la carte graphique Nvidia alors la carte son de
celle-ci ne sera pas correctement ignorée au redémarrage car le module ne sera pas
initialisé correctement. Pour obtenir le comportement correct il faut toujours utiliser la commande `IntelProfile` pour quitter
dès que l'on est sur la carte graphique Nvidia.

```bash
  ### GPU helpers
  # Get current graphic card in use
  alias GPUinfo='glxinfo | grep "OpenGL renderer"'
  # Needed to be able to use alias with sudo
  alias sudo='sudo '
  # Force next restart only to use the Nvidia graphic card and also remove sound card blacklist used tfor PM in intel mode
  alias NvidiaProfile='sudo sudo /etc/optimus-manager/nvidia_sound_card.sh on && optimus-manager --temp-config /etc/optimus-manager/optimus-manager_ForceNvidia.conf && sudo shutdown -h now'
  # Restore Nvidia sound card blacklisting to be able to completely turn of the nvidia card
  alias IntelProfile='sudo sudo /etc/optimus-manager/nvidia_sound_card.sh off && sudo shutdown -h now'
```

**Remarques:**
  - Si `GPUinfo` retourne une carte intel,  `aplay -l | grep NVidia` doit **RIEN** retourner
  - Si `GPUinfo` retourne une carte Nvidia, `aplay -l | grep NVidia` doit retourner la carte associée à la carte graphique Nvidia.




***
### Outils
**Backup:** `sudo pacman -Syu rsync grsync`

**Format disques:** `sudo pacman -Syu gvfs-{afc,goa,google,gphoto2,mtp,nfs,smb} ntfs-3g`

**Image:** `sudo pacman -Syu inkscape gimp gimp-help-fr libreoffice-still-fr eom gnome-screenshot librsvg`

**Son / Video:** `sudo pacman -Syu celluloid lollypop youtube-dl easytag kid3-qt gst-libav gst-plugins-good gst-plugins-base`
Pour une raison obsure **easytag** remplace **nemo** comme programme par défaut pour les dossiers, il faut donc modifier la
configuration dans `Preferred Application`.

**Autres:** `sudo pacman -Syu pandoc pkgstats git p7zip evince`



***
### Terminal
`sudo pacman -Syu gnome-terminal tilix `

Pour avoir **Tilix** toujours actif il faut l'ajouter aux programmes de démarrage.
Ouvrir le menu `StartUp Application` de Cinnamon et ajouter **Tilix** avec `tilix --quake` et 2 secondes
de délai pour éviter de tout charger en même temps.
Aussi possible en ligne de commande `cd Configs && cp Tilix.desktop ~/.config/autostart/`

Ensuite j'aime bien virer le raccourci de **gnome-terminal** et affecter `F4` à l'ouverture **Tilix** (`tilix --quake`).
On change ensuite les paramètres ...

Enfin il est important d'avoir un beau terminal avec [Oh-my-git](https://github.com/arialdomartini/oh-my-git) et
quelques raccourcis.


```bash
    git clone https://github.com/arialdomartini/oh-my-git.git ~/.oh-my-git
    cp Configs/.bashrc ~/.bashrc
```



***
### Internet
`sudo pacman -Syu firefox-i18n-fr megasync`

Ensuite aller à `about:config` dans **Firefox**:

  - `browser.backspace_action` à 0 (backspace --> page précédente)
  - `toolkit.cosmeticAnimations.enabled` à false (vire les animations)



***
### Sublime Text
Sublime Text + support pour root: `pakku -S sublime-text-dev gksu`

Ensuite ajouter les configurations minimales du dossier fournit: `SublimeText` dans `~/.config/sublime-text-3/`
Ouvrir **Sublime Text**:

  - Ajouter la licence
  - Installer [PackageControl](https://packagecontrol.io/installation).

Redémarrer et laisser ouvert le temps d'installer les plugins.



***
### Imprimantes / Scanner
`sudo pacman -Syu cups gutenprint foomatic-db-gutenprint-ppds simple-scan cups-pdf avahi nss-mdns system-config-printer`

Ensuite les services
```bash
    sudo systemctl enable avahi-daemon
    # Will handle starting the service when needed
    sudo systemctl enable org.cups.cupsd.socket
    sudo systemctl enable saned.socket
```

Ensuite il faut modifier certaines configurations:
```bash
    sudo echo "a4" >> /etc/papersize
    # Enable hostname resolution for printer configuration

    sudo subl3 /etc/nsswitch.conf
    # Add < mdns_minimal [NOTFOUND=return] > before resolve and dns

    # Start services
    sudo systemctl start avahi-daemon
    sudo systemctl start org.cups.cupsd.socket
    sudo systemctl start saned.socket
```



***
### Réseaux:
#### Internet
`sudo pacman -Syu wicd wicd-gtk`

On s'ajoute au group **users**: `gpasswd -a pampi users` (pampi étant le nom d'utilisateur).
Un redémarrage est nécessaire lorsque on ajoute un utilisateur à un groupe pour qu'il soit effectif.

Ensuite les services
```bash
    sudo systemctl stop dhcpcd.ens33
    sudo systemctl stop dhcpcd
    sudo systemctl stop NetworkManager
    sudo systemctl disable dhcpcd.ens33
    sudo systemctl disable dhcpcd
    sudo systemctl disable NetworkManager

    sudo systemctl enable wicd
    sudo systemctl start wicd
```

Éviter de mettre à jour **NetworkManager** (dépendance de Cinnamon) en éditant `/etc/pacman.conf`
```
    #IgnorePkg   =
    IgnorePkg   = networkmanager
```

Éviter de charger l'applet pour le NetworkManager ([source](https://wiki.archlinux.org/index.php/Cinnamon#Disable_the_NetworkManager_applet)).
```bash
    cp /etc/xdg/autostart/nm-applet.desktop ~/.config/autostart/nm-applet.desktop
```
Il faut ensuite ajouter à la fin `X-GNOME-UsesNotifications=false` et `X-GNOME-Autostart-enabled=false`.


#### VPN

`sudo pacman -Syu openvpn`

Pour pouvoir se connecter en tant que client il suffit de créer un fichier de configuration avec un nom unique.
```bash
    # copy example configuration to be used as a starting configuration
    sudo cp /usr/share/openvpn/examples/client.conf /etc/openvpn/client/<nom_vpn>.conf
    # Create a file to store username and password to make connection automatic
    cd /etc/openvpn/client/ && touch <nom_vpn>Ids.conf
```

Il est alors possible de l'activer soit manuellement soit en activant le service correspondant
```bash
    # manually
    sudo openvpn /etc/openvpn/client/<nom_vpn>.conf
    # service
    sudo systemctl start openvpn-client@<nom_vpn>.service
```


Il est aussi possible de combiner **wicd** et **openvpn** ([Exemples](https://wiki.archlinux.org/index.php/Wicd#Start/stop_openvpn_client))
Soit les scripts sont ajoutés directement dans les sous-dossiers `/etc/wicd/scripts/`
et seront lancés à chaque connexion. Il est alors nécessaire de trier les types de connections
pour contrôler quand ils s'activeront comme dans le lien ci-dessous.
Soit les scripts sont ajoutés dans les propriétés de chaque connections (*properties--> scripts*).


#### Bluetooth
`sudo pacman -Syu bluez bluez-utils bluez-tools blueman pulseaudio-bluetooth`



***
### Pass
Remplace **passpie** !

Voir:
  - [Arch pass](https://wiki.archlinux.org/index.php/Pass)
  - [Tutoriel complet sur pass](https://sanctum.geek.nz/arabesque/gnu-linux-crypto-passwords/)
  - [Arch GnuPg](https://wiki.archlinux.org/index.php/GnuPG)
  - [Github GnuPg help](https://help.github.com/en/github/authenticating-to-github/generating-a-new-gpg-key)


Allez hop on installe le tout:
```bash
    sudo pacman -S pass

    # Build a gpg key with a trust level of ultimate
    gpg --full-gen-key
    # Get gpg key ID
    gpg --list-secret-keys --keyid-format LONG
    # Export keys as a backup
    mkdir ~/Documents/Passwords && cd ~/Documents/Passwords
    # Export keys
    gpg --export -a <Email or gpg ID> public.key
    gpg --export-secret-keys -a <Email or gpg ID> private.key
    # Protect folder and files with root password
    cd && sudo chown -R root:root ~/Documents/Passwords
```

On initialise le logiciel:
```bash
    # Init database at .password-store
    pass init <gpg ID>
    # Mark .password-store as a git repository
    pass git init
    #
```

Si on veut récupérer les mots de passes depuis un autre gestionnaire:
```bash
    pakku -S pass-import
    # With passpie
    passpie export passwordUnprotected.yml
    pass import passpie passwordUnprotected.yml
```



***
### Battery:
Voir la [Doc](https://wiki.archlinux.org/index.php/Undervolting_CPU)

```bash
    sudo pacman -Syu tlp ethtool smartmontools
    pakku -S tlpui-git intel-undervolt thermald
```

#### TLP
Pour maximiser la performance en étant branché et minimiser la consommation en étant sur batterie
**tlpui** peut être utilisé.
L'ensemble des modifications sont disponible dans le fichier `Configs/Kernel-5.0/tlp`, il suffit donc de remplacer
celui existant:
```bash
  sudo mv /etc/tlp /etc/tlp.old  # backup old one
  sudo cp Configs/Kernel-5.0/tlp /etc
```


Il modifie principalement les valeurs suivantes

  - Processor:
    - CPU_HWP_ON_AC CPU_HWP_ON_BAT
    - CPU_BOOST_ON_AC CPU_BOOST_ON_BAT
    - ENERGY_PERF_POLICY_ON_AC ENERGY_PERF_POLICY_ON_BAT

  - PCIe:
    - RUNTIME_ASPM_ON_AC RUNTIME_ASPM_ON_BAT
    - PCIE_ASPM_ON_BAT
    - RUNTIME_PM_DRIVER_BLACKLIST

  - Sound:
    - SOUND_POWER_SAVE_ON_AC


Il suffit ensuite d'activer les services et d'arrêter ceux que va contrôler `tlp`
```bash
    sudo systemctl mask systemd-rfkill.service
    sudo systemctl mask systemd-rfkill.socket

    sudo systemctl enable tlp-sleep
    sudo systemctl enable tlp
    sudo systemctl start tlp

    # Vérifer les warnings
    tlp-stat -w
```

#### Intel-undervolt
Concernant **intel-undervolt** j'ai tout simplement récupéré les valeurs que j'utilise avec **ThrottleStop** sur Windows.

  - CPU -150mV
  - CPU Cache -150mV
  - GPU -40mV

Afin d'améliorer encore la batterie je me sers des *hints* définis dans **tlp** pour modifier la consommation

  - hwphint switch power:core:gt:8:and:uncore:lt:3 performance balance_performance
  - hwphint switch load:single:0.90 balance_power power

Ces valeurs doivent être ajoutés dans `/etc/intel-undervolt.conf` puis tester avec `sudo intel-undervolt apply`.
Aussi possible en ligne de commande: `cd Configs/ && cp intel-undervolt.conf /etc/ && sudo intel-undervolt apply`.

Si c'est tout bon (tester durant des heures et avec des stress-tests) alors on active le service.
```
    sudo systemctl start intel-undervolt
    sudo systemctl enable intel-undervolt
```

Je pense que c'est une bonne idée de le réinstaller après une mise à jour du noyau mais pas sûr que ça change un truc.


#### Thermald
Ce petit outil permet de mieux gérer la température du petit ordinateur en particulier lors des risques de surchauffes et
limite aussi au maximum l'utilisation trop importante des ventilateurs. Il a un effet préventif en amont des actions drastiques
du driver **pstate** (`cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_driver` pour vérifier qu'il est bien utilisé).

```bash
    sudo systemctl start thermald.service
    sudo systemctl enable thermald.service
```

Je pense que c'est une bonne idée de le réinstaller après une mise à jour du noyau mais pas sûr que ça change un truc.


#### Récap
Avec tout ça j'arrive à près de 10-11h sur battery en IDLE soit une décharge de seulement 6.5W contre 33W
avec la carte intel et luminosité à 50%. En utilisation bureautique / programmation je passe à ~10-11W ce qui
m'offre quand même plus de 8h de batterie. C'est mieux que ce que j'ai sur Windows.




***
### Jeux:
#### Steam
Voir la [Doc](https://wiki.archlinux.org/index.php/Steam)
`sudo pacman -Syu steam libva libvdpau`

Le contrôleur est reconnu directement normalement sur Arch.

Si le controleur n'est pas reconnu directement et il faut suivre les indications
de [ce lien](https://steamcommunity.com/app/353370/discussions/2/1735465524711324558/).


**Notes:**
Pour jouer à des jeux non steam avec le steam controller suivre [ce lien](https://github.com/kozec/sc-controller).

Comme le steam controller dépend de l'overlay pour modifier ces configs on l'a dans le ...
La solution est de modifier la configuration `Desktop` avant de lancer ces jeux
([source](https://wiki.archlinux.org/index.php/Steam#Steam_Controller)).


#### Wine
Voir la [Doc](https://wiki.archlinux.org/index.php/Wine)

Wine: `sudo pacman -Syu wine`

Toutes les dépendances:
```bash
sudo pacman -Syu giflib lib32-giflib libpng lib32-libpng libldap lib32-libldap gnutls lib32-gnutls mpg123 lib32-mpg123 libmpeg2
sudo pacman -S openal lib32-openal v4l-utils lib32-v4l-utils libpulse lib32-libpulse libgpg-error lib32-libgpg-error lib32-gstreamer
sudo pacman -S alsa-plugins lib32-alsa-plugins alsa-lib lib32-alsa-lib libjpeg-turbo lib32-libjpeg-turbo sqlite lib32-sqlite
sudo pacman -S libxcomposite lib32-libxcomposite libxinerama lib32-libgcrypt libgcrypt lib32-libxinerama ncurses lib32-ncurses
sudo pacman -S opencl-icd-loader lib32-opencl-icd-loader libxslt lib32-libxslt libva lib32-libva gtk3 lib32-gtk3
sudo pacman -S gst-plugins-base-libs lib32-gst-plugins-base-libs vulkan-icd-loader lib32-vulkan-icd-loader
```


#### Guild Wars
Télécharger le client à ce [lien](https://www.guildwars.com/fr/download)

Installer le jeu `wine ~/Downloads/GwSetup.exe /complete` et accepter les différentes mises à jour.

Ensuite faire un raccourci avec la commande suivante: `env WINEDEBUG=-all wine "C:\Program Files (x86)\Guild Wars\Gw.exe" -lodfull`

Aussi possible de télécharger l'ensemble du contenu directement avec `WINEPREFIX="/home/pampi/.wine" wine "C:\Program Files (x86)\Guild Wars\Gw.exe" -image`

**Créer le lanceur:**
```
    [Desktop Entry]
    Name=Guild Wars
    Exec=env MESA_GLSL_CACHE_DIR='/home/pampi/.wine/' WINEDEBUG=-all WINEPREFIX="/home/pampi/.wine" wine "C:\Program Files (x86)\Guild Wars\Gw.exe" -lodfull
    Type=Application
    Categories=Game
    StartupNotify=true
    Icon=/home/pampi/Pictures/Icon/GuildWars.png
    NoDisplay=false
    Comment=Guild Wars
    Terminal=false
    Actions=Kill;

    [Desktop Action Kill]
    Exec=env WINEPREFIX='/home/pampi/.wine' wineserver -k
    Name=Force close
```


#### Warhammer Online (RoR)
Télécharger le client à ce [lien](https://www.returnofreckoning.com/join.php)

Installer les dépendances: `sudo pacman -Syu samba lib32-gnutls lib32-libldap`

Il semble que le jeu fonctionne correctement avec un environnement 64bits.

```bash
    WINEARCH=win64 WINEPREFIX=~/.wine64_Warhammer winecfg
    # S'assurer que Windows 7 est utilisé
    WINEPREFIX=~/.wine64_Warhammer winetricks corefonts dotnet40 d3dx9
    WINEPREFIX=~/.wine64_Warhammer wine ~/Downloads/RoRInstaller.exe
```

Un raccourci est automatiquement créé par **Wine** reste juste à faire une copie à la location voulue.

Les bibliothèques suivantes peuvent être nécessaires: `sudo pacman -Syu smbclient libwbclient`

En cas de problèmes, ces posts peuvent aider:

  - [Warhammer install](https://bbs.archlinux.org/viewtopic.php?id=164320)
  - [Battle.Net + Wine](https://appdb.winehq.org/objectManager.php?sClass=version&iId=28855#viewHowTo)
  - [RoR forum](https://www.returnofreckoning.com/forum/viewtopic.php?f=4&t=6043)



#### Guild Wars 2
Voir [Doc Vulkan](https://wiki.archlinux.org/index.php/Vulkan) et [Doc Wine DXVK](https://wiki.archlinux.org/index.php/Wine#DXVK).

Pour améliorer les performances on va utiliser une unité de traduction: DirectX vers Vulkan au lieu d'utiliser DirectX vers OpenGL.


Télécharger le client à ce [lien](https://help.guildwars2.com/hc/en-us/articles/201862968-Download-the-Guild-Wars-2-Client)

Installer les dépendances: `sudo pacman -Syu vulkan-intel vulkan-icd-loader lib32-vulkan-icd-loader`

Télécharger l'unité de traduction à ce [lien](https://github.com/Joshua-Ashton/d9vk/releases)
qui se base sur le projet de base et ajoute la compatibilité avec DirectX9.


Installer le jeu:
```bash
    WINEARCH=win64 WINEPREFIX=~/.wine64_Gw2 winecfg
    # Activer le support pour Vulkan (aller dans le dossier téléchargé)
    WINEPREFIX=~/.wine64_Gw2 ./setup_dxvk.sh install
    # Désactiver CSMT car ce n'est plus utilisé
    WINEPREFIX=~/.wine64_Gw2 winetricks csmt=off
    # Lancer l'installation
    WINEPREFIX=~/.wine64_Gw2 wine ~/Downloads/Gw2Setup-64.exe
    # Ajouter les dossiers
    mkdir -p ~/.wine64_Gw2/dxvk_logs ~/.wine64_Gw2/dxvk_state_cache
```

**Créer le lanceur:**
```
    [Desktop Entry]
    Name=Guild Wars 2
    Exec=env DXVK_LOG_LEVEL='none' DXVK_STATE_CACHE_PATH='/home/pampi/.wine64_Gw2/dxvk_state_cache' MESA_GLSL_CACHE_DIR='/home/pampi/.wine64_Gw2/' WINEDEBUG=-all WINEPREFIX="/home/pampi/.wine64_Gw2" wine C:\\\\windows\\\\command\\\\start.exe /Unix /home/pampi/.wine64_Gw2/dosdevices/c:/ProgramData/Microsoft/Windows/Start\\ Menu/Programs/Guild\\ Wars\\ 2/Guild\\ Wars\\ 2.lnk -dx9single
    Type=Application
    Categories=Game
    StartupNotify=true
    Icon=0637_Gw2-64.0
    StartupWMClass=gw2-64.exe
    Hidden=false
    NoDisplay=false
    Comment=Guild Wars 2
    Terminal=false
    Actions=Kill;

    [Desktop Action Kill]
    Exec=env WINEPREFIX='/home/pampi/.wine64_Gw2' wineserver -k
    Name=Force close
```

Pour aller plus loin, une interface vers DirectX12 est en cours de développement.
Disponible à ce [lien](https://www.reddit.com/r/Guildwars2/comments/ak0mqs/d912pxy_directx12_for_guild_wars_2/?utm_source=share&utm_medium=web2x).


#### WoW
Installer les dépendances:
```bash
    # Vulkan
    sudo pacman -Syu vulkan-intel vulkan-icd-loader lib32-vulkan-icd-loader

    # Common libs
    sudo pacman -Syu lib32-gnutls lib32-libldap lib32-libgpg-error lib32-sqlite lib32-libpulse
    sudo pacman -Syu lib32-gst-plugins-base lib32-gst-plugins-good lib32-gst-plugins-base-libs

    # Support pour DirectX12 (développé par Wine)
    sudo pacman -Syu lib32-vkd3d
```

Télécharger l'unité de transcription pour Vulkan à ce [lien](https://github.com/doitsujin/dxvk/releases) : La version `1.4.4` est fonctionnelle.
Télécharger le client à ce [lien](https://www.battle.net/download/getInstallerForGame?os=win&locale=frFR&version=LIVE&gameProgram=BATTLENET_APP).
Informations pour [dkd3d](https://www.reddit.com/r/wine_gaming/comments/az5b85/vkd3d_world_of_warcraft/eiektpa/).

Le jeu ne fonctionne pas avec la dernière version de *Wine* fournit Arch (4.18) donc j'ai utilisé [Wine-tkg](https://github.com/Tk-Glitch/PKGBUILDS) et reproduit [les étapes](https://lutris.net/games/install/14746/view) faites dans *Lutris*
et aussi utilisé [ces informations](https://wiki.realmofespionage.xyz/games:wine:world_of_warcraft).

**Installer Wine-staging version 4.14:**
```bash
    # Get last sources
    git clone https://github.com/Tk-Glitch/PKGBUILDS.git PKGBUILDS && cd PKGBUILDS/wine-tkg-git/

    # Setup customization using (see customization.cfg)
    # _EXTERNAL_INSTALL="true"
    # _staging_version="v4.14"
    # _use_vkd3d="true"
    # _proton_fs_hack="true"

    # Build it
    # Accept to build with mingw
    # Compilation take close to 4 hours
    makepkg -si
```

**Installer le jeu:**
```bash
    # Créer le dossier
    mkdir -p ~/Wine/BattleNet-64
    # Initialiser le prefix pour Wine
    WINEARCH=win64 WINEPREFIX=~/Wine/BattleNet-64 /opt/wine-tkg-staging-fsync-vkd3d-opt-git/bin/winecfg
    # Set to windows 10 (Application tab)
    # Enable VAAPI as backend for DXVA2 GPU decoding (Staging tab)
    # Ajouter les polices nécessaires
    WINEPREFIX=~/Wine/BattleNet-64 winetricks arial
    # Activer le support pour Vulkan (bien modifier dans le script le lien vers Wine pour correspondre à celui installé --> /opt/wine-tkg-staging-fsync-vkd3d-opt-git/bin/wine)
    # On garde la DLL de wine pour dxgi.dll (nécessaire pour pouvoir utiliser aussi vkd3d (directX12) )
    cd ~/Downloads/dxvk/ && WINEPREFIX=~/Wine/BattleNet-64 ./setup_dxvk.sh install --without-dxgi
    # Ajouter les dossiers pour les logs et le cache
    mkdir -p ~/Wine/BattleNet-64/dxvk_logs ~/Wine/BattleNet-64/dxvk_state_cache
    # Lancer l'installation de BattleNet
    WINEPREFIX=~/Wine/BattleNet-64 /opt/wine-tkg-staging-fsync-vkd3d-opt-git/bin/wine ~/Downloads/Battle.net-Setup.exe
    # Désactiver l’accélération matérielle dans le lanceur (paramètres)
    # NE PAS S'IDENTIFIER mais quitter l'application
    # Relancer avec le lanceur ci-dessous et installer WoW
```

**Créer les lanceurs:**
```
    # BattleNet
    [Desktop Entry]
    Name=Battle.net
    Exec=env DXVK_LOG_LEVEL='none' DXVK_STATE_CACHE_PATH='/home/pampi/Wine/BattleNet-64/dxvk_state_cache' MESA_GLSL_CACHE_DIR='/home/pampi/Wine/BattleNet-64/' WINEDEBUG=-all __GL_SHADER_DISK_CACHE=1 STAGING_SHARED_MEMORY=1 WINEPREFIX="/home/pampi/Wine/BattleNet-64" '/opt/wine-tkg-staging-fsync-vkd3d-opt-git/bin/wine' C:\\\\windows\\\\command\\\\start.exe /Unix /home/pampi/Wine/BattleNet-64/dosdevices/c:/ProgramData/Microsoft/Windows/Start\\ Menu/Programs/Battle.net/Battle.net.lnk
    Type=Application
    Categories=Game
    StartupNotify=true
    Path=/home/pampi/Wine/BattleNet-64/dosdevices/c:/Program Files (x86)/Battle.net
    Icon=56B4_Battle.net Launcher.0
    StartupWMClass=battle.net launcher.exe
    Comment=BattleNet (DXVK Wine 4.14 staging)
    Terminal=false
    Actions=Kill;

    [Desktop Action Kill]
    Exec=env WINEPREFIX='/home/pampi/Wine/BattleNet-64' '/opt/wine-tkg-staging-fsync-vkd3d-opt-git/bin/wineserver' -k
    Name=Force close


    # WoW classic
    [Desktop Entry]
    Name=World of Warcraft Classic
    Exec=env DXVK_LOG_LEVEL='none' DXVK_STATE_CACHE_PATH='/home/pampi/Wine/BattleNet-64/dxvk_state_cache' MESA_GLSL_CACHE_DIR='/home/pampi/Wine/BattleNet-64' WINEDEBUG=-all __GL_SHADER_DISK_CACHE=1 STAGING_SHARED_MEMORY=1 WINEPREFIX='/home/pampi/Wine/BattleNet-64/' '/opt/wine-tkg-staging-fsync-vkd3d-opt-git/bin/wine'  C:\\\\windows\\\\command\\\\start.exe /Unix '/home/pampi/Wine/BattleNet-64/drive_c/Program Files (x86)/World of Warcraft/_classic_/WoW.exe'
    Type=Application
    Categories=Game
    StartupNotify=true
    Path=/home/pampi/Wine/BattleNet-64/dosdevices/c:/Program Files (x86)/World of Warcraft
    Icon=5C12_World of Warcraft Launcher.0
    StartupWMClass=world of warcraft launcher.exe
    Actions=Kill;

    Comment=World of Warcraft Classic (DXVK Wine 4.14 staging)
    Terminal=false

    [Desktop Action Kill]
    Exec=env WINEPREFIX='/home/pampi/Wine/BattleNet-64' '/opt/wine-tkg-staging-fsync-vkd3d-opt-git/bin/wineserver' -k
    Name=Force close


    # Wow Retail
    [Desktop Entry]
    Name=World of Warcraft Retail
    Exec=env DXVK_LOG_LEVEL='none' DXVK_STATE_CACHE_PATH='/home/pampi/Wine/BattleNet-64/dxvk_state_cache' MESA_GLSL_CACHE_DIR='/home/pampi/Wine/BattleNet-64' WINEDEBUG=-all __GL_SHADER_DISK_CACHE=1 STAGING_SHARED_MEMORY=1 WINEPREFIX='/home/pampi/Wine/BattleNet-64/' '/opt/wine-tkg-staging-fsync-vkd3d-opt-git/bin/wine'  C:\\\\windows\\\\command\\\\start.exe /Unix '/home/pampi/Wine/BattleNet-64/drive_c/Program Files (x86)/World of Warcraft/_retail_/WoW.exe'
    Type=Application
    Categories=Game
    StartupNotify=true
    Path=/home/pampi/Wine/BattleNet-64/dosdevices/c:/Program Files (x86)/World of Warcraft
    Icon=5C12_World of Warcraft Launcher.0
    StartupWMClass=world of warcraft launcher.exe
    Actions=Kill;

    Comment=World of Warcraft Retail (DXVK Wine 4.14 staging)
    Terminal=false

    [Desktop Action Kill]
    Exec=env WINEPREFIX='/home/pampi/Wine/BattleNet-64' '/opt/wine-tkg-staging-fsync-vkd3d-opt-git/bin/wineserver' -k
    Name=Force close


```

**Bugs: WOW51900322 streaming error**
Pour corriger problème il faut utiliser un cache provenant d'une machine Windows ... Un est disponible à ce [lien](https://github.com/1thumbbmcc/wowcache)
où dans le dossier `Arch/Aero/Wine`.
Il faut donc supprimer les dossiers `Cache` `Logs` et `WTF` et ajouter celui téléchargé:
```bash
    cd '/home/pampi/Wine/BattleNet-64/drive_c/Program Files (x86)/World of Warcraft/'
    # Nettoie le cache et le remplace par un provenant de Windows...
    rm -Rf _retail_/Cache _retail_/Logs _retail_/WTF
    rm -Rf _classic_/Cache _classic_/Logs _classic_/WTF
    unzip ~/Downloads/wowcache-master.zip
    unzip ~/Downloads/wowcache-master/Cache.zip -d '/home/pampi/Wine/BattleNet-64/drive_c/Program Files (x86)/World of Warcraft/_retail_/'
    unzip ~/Downloads/wowcache-master/Cache.zip -d '/home/pampi/Wine/BattleNet-64/drive_c/Program Files (x86)/World of Warcraft/_classic_/'
```


***
### Protégez vos yeux:
#### Redshift
[Doc](https://wiki.archlinux.org/index.php/Redshift)

```bash
    sudo pacman -Syu redshift
    cp Configs/redshift.conf ~/.config/redshift/
```

Pour avoir **Redshift** toujours actif il faut l'ajouter aux programmes de démarrage.
Ouvrir le menu `StartUp Application` de Cinnamon et ajouter **Redshift** avec `redshift-gtk` et 2 secondes
de délai pour éviter de tout charger en même temps.
Aussi possible en ligne de commande `cp Configs/Redshift.desktop ~/.config/autostart/`


#### Safe Eyes
Forcer des pauses (car on voit pas le temps passer quand on code !) avec [Safe Eyes](http://slgobinath.github.io/SafeEyes/).

```bash
    pakku -S safeeyes
    # Configuration automatic
    cd Configs && cp safeeyes.json ~/.config/safeeyes
```

Équivalent sur Windows avec [Eye Leo](http://eyeleo.com/).




***
### Thèmes:
#### Cinnamon
La configuration finale se trouve dans `Theme/theme.png` tout comme les fichiers du thème.
Les thèmes sont à placer dans `~/.themes` et les curseurs et icônes dans `~/.icons`

Les thèmes de Mint sont disponibles dans le AUR `pakku -S mint-themes` et les autres:

  - [Milix icons](https://www.cinnamon-look.org/p/1259356/)
  - [Tux cursors](https://www.cinnamon-look.org/p/999945/)
  - [Solarized theme](https://www.cinnamon-look.org/p/1315063/)


```bash
    cd Theme
    cp -r Themes ~/.themes
    cp -r Icons ~/.icons
```

#### Grub
[Lien thème original](https://www.gnome-look.org/p/1009236/)

Ajout d'un thème, changement de la résolution, ajout de flags.

```bash
    cd Grub
    sudo cp grub /etc/default/grub
    cd Vimix && sudo ./install_vimix.sh
    # Le script lance déjà cette commande mais au cas où
    sudo grub-mkconfig -o /boot/grub/grub.cfg
```


***
### Problèmes:

#### Mise à jour vers `kernel 5.4.15` et `Nvidia 440.44-5`:

Impossible d'avoir du son en HDMI alors que **Alsa** (`alsa -l`) détecte correctement la carte graphique Nvidia.
Si le noyau `5.5.13` n'est pas encore disponible in faut rester sur `5.4.15`.

```bash
    # Revert to working version of packages
    sudo pacman -U linux-headers-5.4.6.arch1-1-x86_64.pkg.tar.xz linux-5.4.6.arch1-1-x86_64.pkg.tar.xz nvidia-440.44-5-x86_64.pkg.tar.xz nvidia-utils-440.44-1-x86_64.pkg.tar.xz
```


***Correction: Noyau 5.5.13-arch2-1***

Le passage au noyau `5.5.13-arch2-1` semble corriger le problème. Je conserve des erreurs du style
`pci 0000:01:00.1: can't change power state from D3cold to D0 (config space inaccessible)` mais je gagne en batterie
et j'ai le son lorsque je passe sur la carte graphique (nvidia) sans erreurs. Testé en allumant le PC branché et sur batterie.



***
### Autres:

[Arch archives](https://wiki.archlinux.org/index.php/Arch_Linux_Archive)

**Debug**
```bash
    # Last boot systemd logs
    journalctl -b
    # Systemd logs for a specific boot
    # Negative index to start from current boot (-1 --> previous boot)
    journalctl -b <index>
    # Logs for last two hours only for err to alert level
    journalctl --since "2 hour ago" -p err..alert

    # Current xorg logs
    less /var/log/Xorg.0.log
    # Last xorg logs
    less /var/log/Xorg.0.log.old

    # Kernel logs
    dmesg

    # Current session cinnamon logs
    less .xsession-errors
    # Previous session cinnamon logs
    less .xsession-errors.old

    # DKMS kernel status
    dkms status
    # Active modules
    mkinitcpio -M | grep <module name>
    lsmod | grep <module name>
```

**Grub**
  - Rebuild bootloader `sudo grub-mkconfig -o /boot/grub/grub.cfg`


**Initial ramdisk**
  - Rebuild Initial ramdisk `sudo mkinitcpio -P`


**Pacman**
```bash
    # List packages from AUR
    pacman -Qm
    # Get fastest mirrors for packages
    sudo reflector --verbose --latest 100 --sort rate --country France --country Germany --country England --save /etc/pacman.d/mirrorlist.test
    sudo mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.old
    sudo mv /etc/pacman.d/mirrorlist.test /etc/pacman.d/mirrorlist
    sudo pacman -Syyu
```
