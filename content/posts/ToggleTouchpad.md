title: Activer/Désactiver le touchpad sur Gnu-Linux
slug: Activer/Désactiver le touchpad sur Gnu-Linux
category: Astuce
tags: xinput, touchpad, gnu-linux
date: 2020-05-09
modified: 2020-10-09
image: posts/mouse.jpg
status: published

Un petit script pour activer et désactiver le touchpad rapidement et éviter les
mouvement et clics intempestifs !

**Trois étapes à réaliser:**

  - (Optionnel) Trouver l'identifiant correspondant au touchpad
    - `xinput list | grep Touchpad`
  - Copier le script ci-dessous dans un fichier et le rendre exécutable
    - `chmod +x touchpadSwitcher.sh`
  - Définir un raccourci
    - `bash <Chemin vers le script> -i <ID>`


```bash
    #!/bin/bash

    # Toggle ON/OFF input using xinput under the hoods.
    # To get your input ID run: xinput list and look for `Touchpad`

    while getopts ":i:h:" arg; do
      case $arg in
        i) ID=$OPTARG;;
        h) echo "Toggle ON/OFF input based on current state. Use -i to force a specific ID." >&2;;
        \?) echo "Invalid option -$OPTARG" >&2;;
      esac
    done


    #  Automatically find touchpad ID if not provided as an argument
    if [ -z "$ID" ]
      then
        TouchpadID=`xinput list | grep 'Touchpad' | awk '{print $6}' | awk -F '=' '{print $2}'`
    else
        TouchpadID=$ID
    fi


    # Enable or disable it based on its current state
    STATE=`xinput list-props $TouchpadID | grep 'Device Enabled' | awk '{print $4}'`
    if [ $STATE -eq 1 ]
    then
        xinput disable $TouchpadID
        if [ $? -eq 0 ]; then
            echo "Touchpad $TouchpadID now disabled."
        else
            echo "Operation failure"
        fi
    else
        xinput enable $TouchpadID
        if [ $? -eq 0 ]; then
            echo "Touchpad $TouchpadID now enabled."
        else
            echo "Operation failure"
        fi
    fi
```


***
Et voilà !
