title: Latex / Xelatex to PDF/A-1
slug: Latex to PDF-A
category: Tool
tags: tex, pdf, gs
date: 2018-06-03
modified: 2018-06-03
image: posts/latex.jpg
status: published


Afin de pouvoir archiver un PDF, il est nécessaire de s’assurer de la fiabilité de la
diffusion et réduire au maximum la dépendance matérielle. Le format [**PDF/A-1**](https://fr.wikipedia.org/wiki/PDF/A-1)
a été créé dans cet optique et nécessite en autre que toutes les polices soient incorporées
dans le PDF pour garantir sa lisibilité indépendamment du support.

À la fin de la rédaction de ma [thèse](http://www.theses.fr/2017BORD0679) j’ai du passer
mon manuscrit dans ce format pour pouvoir l’archiver de manière pérenne.
Comme de nombreuses personnes dans le milieu scientifique, j’ai utilisé le logiciel [Tex](https://fr.wikipedia.org/wiki/TeX)
pour obtenir un document de qualité (je suis aussi largement plus à l’aise avec un éditeur de texte que avec une usine comme les
[WYSIWYG](https://en.wikipedia.org/wiki/WYSIWYG) tels que LibreOffice Writer).
Afin de profiter de polices exotiques et/ou moins communes j’ai opté [Xetex](http://xetex.sourceforge.net/) qui
a aussi la bonne idée de supporter nativement l’Unicode.

Cependant le paquet [pdfx](https://ctan.org/tex-archive/macros/latex/contrib/pdfx) ne m’a pas
permis de compiler un PDF respectant le standard **PDF/A-1** et j’avais donc besoin d’une autre solution.
Après de nombreuses recherches je suis tombé sur cette commande qui fait le boulot grâce au
logiciel [Ghostscript](https://www.ghostscript.com/).


```bash
    gs -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dCompressFonts=true
       -dSubsetFonts=true -dNOPAUSE -dBATCH -sDEVICE=pdfwrite
       -sOutputFile=output.pdf
       -c ".setpdfwrite <</NeverEmbed [ ]>> setdistillerparams"
       -f original.pdf
```

Étant sur GNU-Linux, Ghostscript est accessible avec `gs` où `original.pdf` est le fichier source et `output.pdf` le fichier de sortie.
Bien sûr les utilisateurs de Windows (on fait pas toujours les meilleurs choix) devront adapter
la commande.
