title: Compresser vos Mp4 rapidement et facilement
slug: Compresser vos Mp4 rapidement et facilement
category: Astuce
tags: mp4, ffmpeg, gnu-linux
date: 2020-05-09
modified: 2020-05-09
image: posts/compression.jpg
status: published

Un petit script pour réduire la taille de vidéos vevant d'internet sans prendre
trop de temps et sans perdre de qualité.
Le script va chercher dans le dossier courant (non récursif) les fichiers finissant par `_Raw.mp4`
et les compresser dans un nouveau fichier sans `_Raw` dans le nom.
```bash
    for i in *_Raw.mp4;
      do name=`echo "$i" | rev | cut -c 9- | rev`
      echo "****************"
      echo "$name"
      echo "****************"
      echo ""
      ffmpeg -i "$i" -c:v libx264 -b:v 1M -c:a mp3 -b:a 128k -crf 25 "${name}.mp4"
    done
```

***
Et voilà !
