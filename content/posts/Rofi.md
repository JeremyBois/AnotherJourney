title: Rofi pour naviguer plus simplement !
slug: RofiAppfinder
category: Tool
tags: tool, finder, launcher, gnu-linux
date: 2020-10-08
modified: 2020-10-08
image: posts/rofi.png
status: published


Je navigue principalement en utilisant les raccourcis clavier et je recherche donc un
moyen de rapidement passer d'une application à une autre ou bien d'en créer une nouvelle.
J'ai donc installé [rofi](https://github.com/davatorium/rofi) qui est disponible sur les
dépôts officiels: `sudo pacman -S rofi`.

J'utilise les configurations suivantes respectivement pour `~/.config/rofi/rofi.rasi`
et `~/.config/rofi/theme.rasi`

```
configuration {
  modi: "drun,window,combi";
  font: "mononoki 13";
  combi-modi: "drun,window";
  icon-theme: "Milix";
  show-icons: true;
  theme: "./themeLight.rasi";
```

```
* {
  background-color: #eee8d5;
  border-color: #586e75;
  text-color: #8ca0aa;
  spacing: 10;
  width: 500px;
  lines: 10;
  columns: 1;
}

inputbar {
  border: 0 0 1px 0;
  children: [prompt,entry];
}

prompt {
  padding: 16px;
  border: 0 0px 0 0;
}

textbox {
  background-color: #eee8d5;
  border: 0 0 1px 0;
  border-color: #586e75;
  padding: 4px 8px;
}

entry {
  padding: 16px;
}

listview {
  cycle: false;
  margin: 0 0 -1px 0;
  scrollbar: false;
}

element {
  border: 0 0 1px 0;
  padding: 8px;
}

element selected {
  background-color: #fdf6e3;
}

```

Pour lancer *rofi* il suffit d'utiliser la commande suivante: `rofi -show combi`


***
Et voilà !
